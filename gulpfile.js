import fs from 'fs';
import gulp from 'gulp';
import sourcemaps from 'gulp-sourcemaps';
import babelify from 'babelify';
import dartSass from 'sass';
import browserSync from 'browser-sync';
import uglify from 'gulp-uglify';
import rename from 'gulp-rename';
import size from 'gulp-size';
import plumber from 'gulp-plumber';
import sassLint from 'gulp-sass-lint';
import gulpSass from 'gulp-sass';
import fileInclude from 'gulp-file-include';
import minHTML from 'gulp-htmlmin';
import replace from 'gulp-string-replace';
import browserify from 'browserify';
import source from 'vinyl-source-stream';
import buffer from 'vinyl-buffer';
import sassvg from 'gulp-sassvg';

const sass = gulpSass(dartSass);
const BrowserSync = browserSync.create();
const timestamp = new Date().getTime();
const isProd = process.env.NODE_ENV === 'production';

/** ****************************************
 ***               REUSABLE FUNCTIONS              ***
 ***************************************** */

/**
 *
 * @param src - This should be the .scss files for build
 * @param cb -  This should be the callback function
 */
const styleCompiler = ({ src, cb }) => {
     gulp.src(src)
          .pipe(sourcemaps.init())
          .pipe(plumber())
          .pipe(sassLint({ configFile: '.sass-lint.yml' }))
          .pipe(sassLint.format())
          .pipe(sass.sync({ outputStyle: 'compressed' }))
          .on('error', sass.logError)
          .pipe(rename({ extname: isProd ? `.min.${timestamp}.css` : '.min.css' }))
          .pipe(sourcemaps.write('./'))
          .pipe(gulp.dest('./build/css/'))
          .pipe(browserSync.stream());

     cb();
};

/**
 *
 * @param src - This should be the .js files for build
 * @param cb -  This should be the callback function
 */
const javascriptCompiler = ({ JSFiles, cb }) => {
     const JSFolder = 'src/project/js/';

     JSFiles.map((entry) =>
          browserify({ entries: [JSFolder + entry] })
               .transform(babelify, { presets: ['env'] })
               .bundle()
               .pipe(source(entry))
               .pipe(rename({ extname: isProd ? `.min.${timestamp}.js` : '.min.js' }))
               .pipe(buffer())
               .pipe(sourcemaps.init({ loadMaps: true }))
               .pipe(uglify())
               .pipe(size())
               .pipe(sourcemaps.write('./'))
               .pipe(gulp.dest('./build/js'))
     );

     cb();
};

/** *************************
 ***               TASKS               ****
 ************************** */
gulp.task('html', () => {
     const htmlSRC = ['./src/project/index.html'];

     return gulp
          .src(htmlSRC)
          .pipe(fileInclude())
          .pipe(replace('js/critical.min.js', isProd ? `js/critical.min.${timestamp}.js` : 'js/critical.min.js'))
          .pipe(replace('js/main.min.js', isProd ? `js/main.min.${timestamp}.js` : 'js/main.min.js'))
          .pipe(replace('css/critical.min.css', isProd ? `css/critical.min.${timestamp}.css` : 'css/critical.min.css'))
          .pipe(replace('css/main.min.css', isProd ? `css/main.min.${timestamp}.css` : 'css/main.min.css'))
          .pipe(size())
          .pipe(minHTML({ collapseWhitespace: true, removeComments: true }))
          .pipe(gulp.dest('./build'));
});

gulp.task('criticalJS', (cb) => {
     const JSFiles = ['critical.js'];

     javascriptCompiler({ JSFiles, cb });
});

gulp.task('nonCriticalJS', (cb) => {
     const JSFiles = ['main.js'];

     javascriptCompiler({ JSFiles, cb });
});

gulp.task('nonCriticalCSS', (cb) => {
     const cssSRC = ['./src/project/scss/main.scss'];

     styleCompiler({ src: cssSRC, cb });
});

gulp.task('criticalCSS', (cb) => {
     const cssSRC = ['./src/project/scss/critical.scss'];

     styleCompiler({ src: cssSRC, cb });
});

gulp.task('svg', () => {
     const rootPath = './src/project/assets/svg';
     const svgSRC = [`${rootPath}/*.svg`];
     const pathToFolder = `${rootPath}/sassvg`;

     const hasFolder = fs.existsSync(pathToFolder);

     if (!hasFolder) {
          fs.mkdirSync(pathToFolder);
     }

     return gulp
          .src(svgSRC)
          .pipe(plumber())
          .pipe(sassvg({ outputFolder: pathToFolder }))
          .pipe(browserSync.stream());
});

gulp.task('server', (cb) => {
     BrowserSync.init({ server: { baseDir: 'build' } });
     cb();
});

gulp.task('watchTask', () => {
     const tasksToWatch = ['html', 'criticalCSS', 'criticalJS', 'nonCriticalCSS', 'nonCriticalJS'];

     gulp.watch(['src/default/scss/**/*.scss', 'src/project/scss/**/*.scss'], gulp.series(tasksToWatch)).on('change', BrowserSync.reload);
     gulp.watch(['src/default/**/*.js', 'src/project/js/critical.js'], gulp.series(tasksToWatch)).on('change', BrowserSync.reload);
     gulp.watch(['src/default/**/*.js', 'src/project/js/main.js'], gulp.series(tasksToWatch)).on('change', BrowserSync.reload);
     gulp.watch(['src/project/**/*.html'], gulp.series(tasksToWatch)).on('change', BrowserSync.reload);
});

gulp.task('default', gulp.series('html', 'criticalJS', 'nonCriticalJS', 'criticalCSS', 'nonCriticalCSS', 'server', 'watchTask'));
